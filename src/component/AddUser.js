import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

const AddUser = () => {
    const navigate = useNavigate();
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [gender, setGander] = useState("Male")

    const saveUser = async ( e ) => {
        e.preventDefault();
        try {
            await axios.post( 'http://localhost:5000/users', {
                name,
                email,
                gender,
            } );
            navigate("/")
        } catch (error) {
            console.log(error)
        }
    }

  return (
    <div className="columns mt-5 is-centered">
          <div className="column is-half">
              <form onSubmit={saveUser}>
                  <div className="field">
                      <label className="label">
                        Nama
                      </label>
                      <div className="control">
                          <input
                              type="text"
                              className="input"
                              value={ name }
                              onChange={(e)=> setName(e.target.value)}
                              placeholder='name' />
                      </div>
                  </div>
                  <div className="field">
                      <label className="label">
                        Email
                      </label>
                      <div className="control">
                          <input
                              type="text"
                              className="input"
                              value={ email }
                              onChange={(e)=> setEmail(e.target.value)}
                              placeholder='name' />
                      </div>
                  </div>
                  <div className="field">
                      <label className="label">
                        gender
                      </label>
                      <div className="control">
                          <div className="select is-fullwidth">
                              <select
                                  value={ gender }
                                  onChange={(e)=> setGander(e.target.value)}
                              >
                                  <option value="Male">Male</option>
                                  <option value="Female">FeMale</option>
                              </select>
                         </div>
                      </div>
                  </div>
                  <div className="field">
                     <button type='submit' className="button is-success" >save</button>
                  </div>
              </form>
        </div>
    </div>
  )
}

export default AddUser
